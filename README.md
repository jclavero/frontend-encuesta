--stack--

Java v 11
Spring v 2.4.3
Maven v 3.6.3
Node v 18
Angular v 15

Api Front
Instalar cliente angular :
npm install -g @angular/cli

Ir a la carpeta del proyecto y levantar la app por consola o IDE :
ng serve -o

Api Back
Tener instalada la version de java 11 pre ferentemente
Tener instalado maven version 3.6.3 preferentemente
Levantar con IDE preferentemente para evitar problemas con el tomcat embebido o la bd H2 en memoria.
Compilar y levantar por consola de comandos:

mvn clean install -Dmaven.test.skip=true
mvn spring-boot:run