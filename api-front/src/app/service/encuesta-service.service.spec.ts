import { HttpClientModule } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { EncuestaServiceService } from './encuesta-service.service';
import { MatCardModule } from '@angular/material/card';
import { ReactiveFormsModule } from '@angular/forms';
import { MatNativeDateModule } from '@angular/material/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Resultado } from '../model/resultado';
import { Encuesta } from '../model/encuesta';

describe('EncuestaServiceService', () => {
  let service: EncuestaServiceService;

  let httpClientSpy: { get : jasmine.Spy , post: jasmine.Spy};
  let mockedArray: any[] = ['array', 'of', 'anything'];
  let mockEncuesta: Encuesta;
  let mockResultado: Resultado[];

  const mockedSmsService: {
    saveFormulario: () => Observable<any>,
    getResultados: () => Observable<any>
  } = {
    saveFormulario: () => of(mockedArray),
    getResultados: () => of(mockedArray)
  };

  beforeEach(async () => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientModule,
        HttpClientTestingModule,
        MatCardModule,
        ReactiveFormsModule,
        MatNativeDateModule,
        BrowserAnimationsModule
      ]
    }).compileComponents();
   
    service = TestBed.inject(EncuestaServiceService);
  });

  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['post','get']);
    service = new EncuestaServiceService(httpClientSpy as any);
})

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should list result', (done: DoneFn) => {
    httpClientSpy.get.and.returnValue(of(mockResultado))
    service.getResultados()
      .subscribe(resultado => { 
        expect(resultado).toEqual(mockResultado)
        done()
      })
  });

  it('should send form', (done: DoneFn) => {
    httpClientSpy.post.and.returnValue(of(mockEncuesta))
    service.saveFormulario(mockEncuesta)
      .subscribe(resultado => { 
        expect(resultado).toEqual(mockEncuesta)
        done()
      })
  });

});
