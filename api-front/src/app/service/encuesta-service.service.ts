import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Encuesta } from '../model/encuesta';
import { Resultado } from '../model/resultado';

@Injectable({
  providedIn: 'root'
})
export class EncuestaServiceService {
  
  private url: string;

  constructor(private http: HttpClient) {
    this.url = 'http://localhost:8080';
  }

  public saveFormulario(encuesta: Encuesta) {
    const sufix = '/encuesta';
    return this.http.post<Encuesta>(this.url+sufix, encuesta);
  }

  public getResultados(): Observable<Resultado[]> {
    const sufix = '/resultados';
    return this.http.get<Resultado[]>(this.url+sufix);
  }
}
