import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Encuesta } from '../model/encuesta';
import { EncuestaServiceService } from '../service/encuesta-service.service';

@Component({
  selector: 'app-formulario',
  templateUrl: './formulario.component.html',
  styleUrls: ['./formulario.component.css']
})
export class FormularioComponent {

  public form!: FormGroup;
  constructor(private formBuilder: FormBuilder, private service:EncuestaServiceService, private _snackBar: MatSnackBar){}

  ngOnInit(): void{
    this.loadForm();  
  }

  send():any{
    let encuesta: Encuesta;
    encuesta=this.form.value;
    console.log(encuesta);
    this.service.saveFormulario(encuesta).subscribe(data => {
      this.openSnackBar('Formulario Enviado','Salir');
    });
  }

  openSnackBar(message: string, action: string) {
    this.loadForm(); 
    this._snackBar.open(message, action);
  }

  loadForm(): any{
    this.form = this.formBuilder.group(
      {
        correo: ['',[Validators.required,Validators.email]],
        estilo: ['',[Validators.required]]
      });

  }

}
