import { Component, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Resultado } from '../model/resultado';
import { EncuestaServiceService } from '../service/encuesta-service.service';

export interface PeriodicElement {
  estilo: string;
  total: number;
}

const ELEMENT_DATA: PeriodicElement[] = [
  {estilo: 'Rock', total: 1},
  {estilo: 'Pop', total: 4},
  {estilo: 'Jazz', total: 6},
  {estilo: 'Clásica', total: 9},
  {estilo: 'etc', total: 10}
];

@Component({
  selector: 'app-resultado',
  templateUrl: './resultado.component.html',
  styleUrls: ['./resultado.component.css']
})
export class ResultadoComponent {

  
  resultado!: Resultado[];
  constructor(private service:EncuestaServiceService){};


  displayedColumns: string[] = ['estilo', 'total'];
  dataSource = new MatTableDataSource<PeriodicElement>(ELEMENT_DATA);
  

  @ViewChild(MatPaginator)
  paginator!: MatPaginator;

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  ngOnInit(): void{
    this.loadData();  
  }
  
  loadData(): any{
    this.service.getResultados().subscribe(response => {
      this.resultado = response;
      this.dataSource = new MatTableDataSource<PeriodicElement>(this.resultado);
    });
  }
}
