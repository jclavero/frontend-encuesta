import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { MatCardModule } from '@angular/material/card';
import { MatNativeDateModule } from '@angular/material/core';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatTableModule } from '@angular/material/table';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Observable, of } from 'rxjs';
import { EncuestaServiceService } from '../service/encuesta-service.service';

import { ResultadoComponent } from './resultado.component';

describe('ResultadoComponent', () => {
  let component: ResultadoComponent;
  let fixture: ComponentFixture<ResultadoComponent>;

  let mockedArray: any[] = ['array', 'of', 'anything'];
  let emptyArray: any[] = [];

    const mockedService: {
      saveFormulario: () => Observable<any>,
      getResultados: () => Observable<any>
    } = {
      saveFormulario: () => of(mockedArray),
      getResultados: () => of(mockedArray),
    };
    
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        MatSnackBarModule,
        MatCardModule,
        ReactiveFormsModule,
        MatNativeDateModule,
        MatTableModule,
        MatPaginatorModule,
        BrowserAnimationsModule,
        MatFormFieldModule,
      ],
      declarations: [ ResultadoComponent ],
      providers: [
        { provide: EncuestaServiceService, useValue: mockedService }
    ],
    })
    .compileComponents();

    fixture = TestBed.createComponent(ResultadoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
